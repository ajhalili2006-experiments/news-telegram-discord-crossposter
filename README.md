# News Telegram-Discord Crossposter

A Pyrogram project for broadcasting news on both Telegram and Discord.

## Setup

TBD

## Required Environment Variables

* `TG_BOT_TOKEN`
* `DISCORD_WEBHOOK_ID` and `DISCORD_WEBHOOK_SECRET`
* `TG_CHANNEL_ID` for Telegram chat ID to beam notifications on Telegram side.
* `PORT` for

## License

MIT
